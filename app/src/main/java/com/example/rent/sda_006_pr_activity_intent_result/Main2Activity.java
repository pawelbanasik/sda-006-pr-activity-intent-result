package com.example.rent.sda_006_pr_activity_intent_result;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main2Activity extends AppCompatActivity {

    @BindView(R.id.edit_text)
    protected EditText editTextOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
    }
    @OnClick (R.id.button_two)
    public void activity_2_click(View v) {
        Intent intent = new Intent();
        intent.putExtra("tekst", editTextOne.getText().toString());
        setResult(RESULT_OK, intent);
        finish();

    }
}
