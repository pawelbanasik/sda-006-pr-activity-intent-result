package com.example.rent.sda_006_pr_activity_intent_result;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button_one)
    protected Button buttonOne;

    @BindView(R.id.text_view_one)
    protected TextView textViewOne;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    // ta metoda jest generowana
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && data.hasExtra("tekst")) {
            textViewOne.setText(data.getStringExtra("tekst"));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick (R.id.button_one)
    public void activity_1_click(View v) {
        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
        startActivityForResult(intent, 1);
    }
}
